%bcond_with bootstrap

Name:           python-xmltodict
Version:        0.14.1
Release:        1
Summary:        Python module that makes working with XML feel like you are working with JSON 
License:        MIT
URL:            https://github.com/martinblech/xmltodict
Source0:        https://files.pythonhosted.org/packages/98/f7/d29b8cdc9d8d075673be0f800013c1161e2fd4234546a140855a1bcc9eb4/xmltodict-0.14.1.tar.gz

BuildArch:      noarch

%description
Python module that makes working with XML feel like you are working with JSON

%package -n python3-xmltodict
Summary:        Python module that makes working with XML feel like you are working with JSON 
BuildRequires:       python3-pytest expat-devel
BuildRequires:       python3-devel
BuildRequires:       python3-setuptools
%{?python_provide:%python_provide python3-xmltodict}

%description -n python3-xmltodict
Python module that makes working with XML feel like you are working with JSON 

%prep
%autosetup -n xmltodict-%{version}

%build
%py3_build

%install
%py3_install

%check
%{__python3} -m pytest tests

%files -n python3-xmltodict
%doc README.md
%license LICENSE
%{python3_sitelib}/*


%changelog
* Tue Oct 15 2024 lixiaoyong <lixiaoyong@kylinos.cn> - 0.14.1-1
- Upgrade package to version 0.14.1
  - Drop support for Python older than 3.6
  - Additional ruff/Pyflakes/codespell fixes

* Wed Jun 29 2022 caodongxia <caodongxia@h-partners.com> - 0.13.0-1
- Update to 0.13.0

* Tue Mar 8 2022 baizhonggui <baizhonggui@huawei.com> - 0.12.0-4
- add expat-devel build require to fix build error

* Fri Sep 17 2021 wangyue <wangyue92@huawei.com> - 0.12.0-3
- add provide python-xmltodict

* Tue Aug 17 2021 wulei <wulei80@huawei.com> - 0.12.0-2
- add buildrequires

* Tue Apr 28 2020 Wei Xiong  <myeuler@163.com>
- init package
